Repository to build [emoji-search](https://github.com/ahfarmer/emoji-search) React app with Docker, GitLab CI/CD services and run it in Kubernetes.

## Requirements
- git
- Docker
- kubectl
- [Tilt](https://docs.tilt.dev/)

## Local Development
### Docker
Build the React app and package static content as nginx image:
```
# build an image for the react app
docker build \
    --build-arg COMMIT_SHA=$(git rev-parse HEAD) \
    --tag app:latest -f docker/Dockerfile .

# serve the web app on port 80
docker run -d -p 80:80 app:latest
```

### Kubernetes
Kubernetes resources were created to deploy the app image as a 2-replica StatefulSet and expose via Service:
```
kubectl apply -f kubernetes/

kubectl get svc,sts
```

### Tilt
[Tilt](https://docs.tilt.dev/) is used to automate the local development simplifying repeatable Docker and K8s operations:
```
tilt up
```
Follow the instructions suggested by Tilt.

## Security
### Addressed
- nginx web server config includes headers to improve security around XSS and Clickjacking protection
- nginx web server doesn't expose its version via headers and any docs/status pages
- this specific simple web app is HTTP GET only, block irrelevant request methods from users
### Unaddressed
- [emoji-search](https://github.com/ahfarmer/emoji-search) has outdated or deprecated npm dependencies with `5 high severity vulnerabilities`
  - can't be used in production until proper refactoring and package.json/yarn.lock regeneration
- SSL termination is out of scope of this task

## CI/CD
- [GitlabCI pipeline](.gitlab-ci.yml)
  - `test` - runs npm test for the React app
  - `review` - produces a generated app preview for every PR via [Gitlab review apps](https://docs.gitlab.com/ee/ci/review_apps/) and [Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/)
  - `build` - build and package static app files as a nginx docker image
  - `deploy` - push produced app image to [Gitlab container registry](https://gitlab.com/armab/eugen-21-feb-2021/container_registry) (only `master`)

## Future Improvements
- Security: content trust in Docker and image signing & verification: <https://docs.docker.com/engine/security/trust/>
- Security: docker image scanning via automated tools for security best practices, outdated software
- Security: package own nginx Dockerfile instead of using upstream/official one from Docker Hub (?)
- Security: explore `Content-Security-Policy` for nginx responses <https://content-security-policy.com/>
- CI: automated CI check to verify if any of the npm dependencies are potentially vulnerable (npm audit)
- CI: Using custom Docker image for runner
- CI: Use of YAML anchors in .gitlab-ci.yml file
